package adapter;

public interface AlternativeList {

    Object pop();

    void push(Object obj);

}
