package adapter;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<String> list = new ArrayList<String>();
        AlternativeList listAdapter = new ListAdapter(list);

        listAdapter.push("first");
        listAdapter.push("second");
        listAdapter.push("third");
        System.out.println(listAdapter.pop());

        for (String s : list) {
            System.out.println(s);
        }



    }
}
