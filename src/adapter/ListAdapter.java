package adapter;

import java.util.List;

public class ListAdapter implements AlternativeList {
    List list; // adaptee

    public ListAdapter(List list) {
        this.list = list;
    }


    @Override
    public Object pop() {
        return list.get(list.size() - 1);
    }

    @Override
    public void push(Object obj) {
        list.add(obj);
    }
}
