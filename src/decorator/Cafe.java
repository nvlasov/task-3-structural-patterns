package decorator;

public class Cafe
{
	public static void main(String[] args)
	{
		Beverage beverage = new Espresso();
		System.out.println(beverage.getDescription() + " $" + beverage.cost());

		Beverage beverage2 = new Milk(new Milk(new Mocha(new HouseBlend())));
		System.out.println(beverage2.getDescription() + " $" + beverage2.cost());

	}
}
